export class Greetings {

    constructor() {
    }

    public greet(name: string) {
        return 'Hello, ' + name + '!';
    }
}