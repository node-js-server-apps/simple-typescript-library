export function helloWorld() {
    return "Hello World!"
}

export function greetWorld(name: string) {
    return 'Hello, ' + name + '!';
}
